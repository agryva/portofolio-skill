import { FormGeneralProps } from "@/interfaces/form.interface"
import { FC } from "react"
import { useFormContext } from "react-hook-form"
import { FieldControlForm } from "."
import { Textarea } from "../ui/textarea"


const TextAreaForm: FC<FormGeneralProps> = ({
    name,
    label,
    placeholder,
    className,
    description,
    type = 'text',
    labelClassName
}) => {
    const methods = useFormContext()


    return (
        <FieldControlForm
            control={methods.control}
            name={name}
            label={label}
            description={description}
            labelClassName={labelClassName}
            render={(props) => {
                return (
                    <Textarea
                        {...props}
                        className={className}
                        inputMode={type}
                        placeholder={placeholder}
                        onWheel={(event) => event.currentTarget.blur()}
                    />
                )
            }}
        />
    )
}

export default TextAreaForm