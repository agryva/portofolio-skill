
import { cn } from '@/lib/utils'
import fileAnimation from '@/public/lottie/upload-new.json'

import Lottie from 'lottie-react'
import { FC, useCallback, useState } from 'react'
import { FileRejection, useDropzone } from 'react-dropzone'


import { Progress } from '../ui/progress'
import { FileTypeCustom } from '@/constants/fileType'
import fileUtils from '@/lib/file'
import { sleep } from '@/lib/sleep'
import { toast } from "sonner"

interface UploadFieldProps {
    fileOnChange?: (file: File) => void
    fileType?: string[]
}

const UploadField: FC<UploadFieldProps> = ({ fileOnChange, fileType }) => {
    const [file, setFile] = useState<any>(null)
    const [progress, setProgress] = useState(20)
    const [fileName, setFileName] = useState<string | null>(null)
    const [isLoading, setIsLoading] = useState<boolean>(false)

    const onDrop = useCallback(async (acceptedFiles: File[], fileRejections: FileRejection[]) => {
        try {
            setProgress(0)
            if (fileRejections.length > 0) {
                let errorMessage: string[] = []
                fileRejections.forEach((reject) => {
                    reject.errors?.forEach((error) => {
                        errorMessage.push(error.message)
                    })
                })

                toast.error(errorMessage.join(', '))
                return
            }

            if (acceptedFiles.length > 0) {
                const file = acceptedFiles[0]
                setFileName(file.name)
                await sleep(500)
                setProgress(60)
                setFile(file)
                setIsLoading(true)
                await sleep(700)
                setProgress(100)
                if (fileOnChange) fileOnChange(file)
                setIsLoading(false)
            }
        } catch (error: any) {
            toast.error(error)
        }
    },
        [],
    )

    const tranformFileType = (fileType?: string[]) => {
        const result: any = {}

        if (fileType) {
            for (const key in FileTypeCustom) {
                for (const mimeType in FileTypeCustom[key]) {
                    const extensions = FileTypeCustom[key][mimeType]
                    if (extensions.some((ext: any) => fileType.includes(ext))) {
                        result[mimeType] = extensions.filter((ext: any) =>
                            fileType.includes(ext),
                        )
                    }
                }
            }

            return result
        } else {
            return {
                'image/*': ['.jpeg', '.png', '.jpg', '.webp'],
            }
        }
    }

    const { getRootProps, getInputProps } = useDropzone({
        onDrop,
        maxFiles: 1,
        accept: tranformFileType(fileType),
        maxSize: 5242880,
    })


    return (
        <div
            {...getRootProps()}
            className="flex flex-col justify-center w-full whitespace-normal overflow-hidden text-ellipsis"
        >
            <label
                htmlFor="dropzone-file"
                className="flex flex-col items-center justify-center w-full h-64 border border-primary border-dashed rounded-lg cursor-pointer bg-white hover:bg-gray-100 "
            >
                <div className="flex flex-col items-center justify-center pt-5 pb-6 ">
                    {fileName === null ? (
                        <>
                            <div className="w-[130px]">
                                <Lottie animationData={fileAnimation} loop={true} />
                            </div>
                            <div className="mb-8 mt-2">
                                <p className="text-sm text-gray-500">
                                    <span className="font-semibold">
                                        Drag and drop files here
                                    </span>{' '}
                                    or search for files

                                </p>
                                <p className='text-[0.8rem] text-center text-[#ADADAD] '>Maximum Size: {fileUtils.fileSizeFormat(5242880)}</p>
                            </div>
                        </>
                    ) : (
                        <>
                            <div className="w-[130px]">
                                <Lottie animationData={fileAnimation} loop={true} />
                            </div>
                            <p className="mb-8 mt-2 text-sm text-gray-500 dark:text-gray-400 text-center whitespace-normal overflow-hidden text-ellipsis">
                                <span className="font-semibold text-center whitespace-normal overflow-hidden text-ellipsis line-clamp-1">
                                    {fileName}
                                </span>
                            </p>
                        </>
                    )}
                </div>
                <input {...getInputProps()} />
            </label>

            <div
                className={
                    cn('w-full p-4 gap-1 mt-4 border flex flex-col hover:bg-gray-100 border-gray-300 rounded-md animate__animated animate__fadeIn',
                        isLoading ? 'flex' : 'hidden')
                }>
                <div className="flex justify-between">
                    <p className='text-xs text-gray-500 font-normal'>Uploading...</p>
                    <p className='text-xs text-gray-500 font-semibold'>{progress}%</p>
                </div>

                <Progress
                    value={progress}
                    className="w-full"
                />
            </div>

        </div>
    )
}

export default UploadField
