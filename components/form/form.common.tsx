import { DetailedHTMLProps, FC, FormHTMLAttributes, ReactElement, ReactNode } from 'react'
import { FormProvider, UseFormReturn } from 'react-hook-form'

type FormProps = {
    children: ReactNode | ReactNode[]
    form: UseFormReturn
    fullWidth?: boolean
    classNames?: string
} & DetailedHTMLProps<FormHTMLAttributes<HTMLFormElement>, HTMLFormElement>

const FormCommon: FC<FormProps> = ({
    children,
    form,
    fullWidth = true,
    classNames = '',
    ...rest
}): ReactElement => {
    return (
        <FormProvider {...form}>
            <form
                {...rest}
                className={`${fullWidth ? 'w-full' : ''} ${classNames}`}
                style={{ ...rest.style }}
            >
                {children}
            </form>
        </FormProvider>
    )
}

export default FormCommon
