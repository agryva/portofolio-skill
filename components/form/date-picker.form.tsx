import { FormGeneralProps } from "@/interfaces/form.interface"
import { FC, useState } from "react"
import { useFormContext } from "react-hook-form"
import { FieldControlForm } from "."
import { format, parse } from 'date-fns'
import DatePickerField from "./date-picker.field"

const DatePickerForm: FC<FormGeneralProps> = ({
    name,
    label,
    placeholder,
    className,
    description,
    type = 'text',
    labelClassName
}) => {
    const methods = useFormContext()

    const setDefaultValue = (propsValue: string | undefined) => {
        try {
            if (propsValue) {
                return parse(propsValue, 'dd MMMM yyyy', new Date())
            }

            return undefined
        } catch (error: any) {
            return propsValue
        }
    }
    return (
        <FieldControlForm
            control={methods.control}
            name={name}
            label={label}
            description={description}
            labelClassName={labelClassName}
            render={(props) => {
                return (
                    <DatePickerField
                        defaultValue={setDefaultValue(props.value)}
                        onDateChange={(date) => {
                            if (date) {
                                props.onChange(format(date, 'dd MMMM yyyy'),)
                            }
                        }}
                    />
                )
            }}
        />
    )
}

export default DatePickerForm