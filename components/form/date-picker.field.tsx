import { Button } from '@/components/ui/button'
import { Calendar } from '@/components/ui/calendar'
import { Popover, PopoverContent, PopoverTrigger } from '@/components/ui/popover'
import { cn } from '@/lib/utils'
import { format } from 'date-fns'
import { Calendar as CalendarIcon } from 'lucide-react'
import { FC, useEffect, useState } from 'react'
import { Matcher } from 'react-day-picker'

interface DatePickerProps {
  className?: string
  disabled?: boolean
  onDateChange?: (date: Date | undefined) => void
  defaultValue?: any
  dateFormat?: string
  disabledDays?: Matcher | Matcher[] | undefined
  fromDate?: Date
}

const DatePickerField: FC<DatePickerProps> = ({
  className = '',
  disabled,
  onDateChange,
  defaultValue,
  dateFormat,
  disabledDays,
  fromDate,
}) => {
  const [open, setOpen] = useState(false)
  const [date, setDate] = useState<Date>()
  const [isFirstTime, setIsFirstTime] = useState<boolean>(true)

  useEffect(() => {
    if (isFirstTime) {
      if (defaultValue) {
        setDate(defaultValue)
        setIsFirstTime(false)
      }
    }
  }, [defaultValue])

  const convertToReadableFormat = () => {
    try {
      // Check if the date variable is defined
      if (date) {
        // Use the format function to format the date
        // If dateFormat is not provided, it defaults to 'dd MMMM yyyy'
        return format(date, dateFormat || 'dd MMMM yyyy')
      } else {
        // If date is not defined, return a hyphen
        return '-'
      }
    } catch (error: any) {
      // If an error occurs during formatting, return a hyphen
      return '-'
    }
  }

  return (
    <Popover open={open} onOpenChange={setOpen}>
      <PopoverTrigger asChild>
        <Button
          variant={'outline'}
          disabled={disabled}
          className={cn(
            'w-full border-[#D4D4D4] text-[#171717] justify-start text-left font-normal overflow-hidden truncate rounded-lg',
            !date && 'text-muted-foreground',
            className,
          )}
        >
          <CalendarIcon className="mr-2 h-4 w-4" />
          {date ? (
            <span className="overflow-hidden truncate text-xs">
              {convertToReadableFormat()}
              {/* {date} */}
            </span>
          ) : (
            <span className="overflow-hidden truncate text-xs">
              Select Date
            </span>
          )}
        </Button>
      </PopoverTrigger>
      <PopoverContent className="w-auto p-0 z-[99992]">
        <Calendar
          mode="single"
          selected={date}
          disabled={disabledDays}
          fromDate={fromDate}
          onSelect={(date) => {
            setDate(date)
            if (onDateChange) {
              onDateChange(date)
            }
            setOpen(false)
          }}
          initialFocus
        />
      </PopoverContent>
    </Popover>
  )
}

export default DatePickerField
