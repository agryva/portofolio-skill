import { FormGeneralProps } from "@/interfaces/form.interface"
import { FC, useState } from "react"
import { useFormContext } from "react-hook-form"
import { FieldControlForm } from "."
import { Input } from "../ui/input"

const InputForm: FC<FormGeneralProps> = ({
    name,
    label,
    placeholder,
    className,
    description,
    type = 'text',
    labelClassName
}) => {
    const methods = useFormContext()

    return (
        <FieldControlForm
            control={methods.control}
            name={name}
            label={label}
            description={description}
            labelClassName={labelClassName}
            render={(props) => {
                return (
                    <Input
                        {...props}
                        className={className}
                        type={type}
                        pattern={type === 'number' ? '[0-9]*' : undefined}
                        inputMode={type}
                        placeholder={placeholder}
                        onWheel={(event) => event.currentTarget.blur()}
                    />
                )
            }}
        />
    )
}

export default InputForm