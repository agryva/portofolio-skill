import { cn } from '@/lib/utils'
import { Roboto } from 'next/font/google'
import { FC, ReactNode } from 'react'

import { FormControl, FormDescription, FormField, FormItem, FormLabel, FormMessage } from '../ui/form'

interface FieldControlProps {
  name: string
  label?: string | ReactNode
  labelClassName?: string
  description?: string
  render: (args?: any) => ReactNode
  control: any
}

const roboto = Roboto({ subsets: ['latin'], weight: ['400', '500', '700'] })

const FieldControlForm: FC<FieldControlProps> = ({
  name,
  render,
  label,
  control,
  description,
  labelClassName
}) => {
  return (
    <FormField
      name={name}
      control={control}
      render={({ field }) => {
        return (
          <FormItem className="space-y-2">
            {label && (
              <div className="flex flex-col gap-1" style={{ marginBottom: 4 }}>
                {label && (
                  <FormLabel
                    className={cn(" placeholder:text-muted-foreground focus-visible:ring-ring disabled:cursor-not-allowed disabled:opacity-50 px-0 text-sm font-normal ", labelClassName, roboto.className)}
                    htmlFor={name}
                  >
                    {label}
                  </FormLabel>
                )}
                {description && (
                  <FormDescription className="text-xs font-medium text-color-off dark:text-color-off-dark">
                    {description}
                  </FormDescription>
                )}
              </div>
            )}

            <FormControl>
              {render({
                ...field,
                id: name,
              })}
            </FormControl>

            <FormMessage />
          </FormItem>
        )
      }}
    />
  )
}

export default FieldControlForm
