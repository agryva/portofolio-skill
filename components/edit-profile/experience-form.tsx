'use client'

import * as yup from "yup"
import { yupResolver } from "@hookform/resolvers/yup"
import { UseFormReturn, useForm } from "react-hook-form"
import { FormCommon, InputForm } from "../form"
import DatePickerForm from "../form/date-picker.form"
import TextAreaForm from "../form/textarea.form"
import { Button } from "../ui/button"
import { useProfileStore } from "@/stores"

const experienceSchema = yup.object({
    position: yup.string().required('Position is required'),
    company: yup.string().required('Company is required'),
    startDate: yup.string().required('Start Date is required'),
    endDate: yup.string().required('End Date is required'),
}).required()


const ExperienceForm = ({ onClose }: { onClose: () => void }) => {

    const { addExperience } = useProfileStore()

    const form = useForm<any>({
        resolver: yupResolver(experienceSchema),
        defaultValues: {},
    })

    const submit = async () => {
        const isValid = await form.trigger()
        if (!isValid) return
        addExperience(form.getValues())
        onClose()
    }

    return (
        <div className="w-full">
            <FormCommon
                classNames="flex flex-col mt-4 gap-4 w-full"
                form={form as unknown as UseFormReturn}
                onSubmit={form.handleSubmit(submit)}
            >
                <div>
                    <InputForm
                        type="text"
                        name="position"
                        placeholder="xxxx"
                        label={'Position'}
                        className="dark:bg-[#111827]"

                    />
                </div>
                <div>
                    <InputForm
                        type="text"
                        name="company"
                        placeholder="xxxx"
                        label={'Company'}
                        className="dark:bg-[#111827]"

                    />
                </div>
                <div>
                    <DatePickerForm
                        type="text"
                        name="startDate"
                        placeholder="xxxx"
                        label={'Start Date'}
                        className="dark:bg-[#111827]"

                    />
                </div>
                <div>
                    <DatePickerForm
                        type="text"
                        name="endDate"
                        placeholder="xxxx"
                        label={'End Date'}
                        className="dark:bg-[#111827]"

                    />
                </div>
                <div>
                    <TextAreaForm
                        name="description"
                        placeholder="xxxx"
                        label={'Description'}
                        className="dark:bg-[#111827]"
                    />
                </div>
                <div>
                    <Button
                        onClick={submit}
                        type="button"
                    >
                        Save
                    </Button>
                </div>
            </FormCommon>
        </div>
    )
}

export default ExperienceForm