'use client'

import {
    Card,
    CardContent,
} from "@/components/ui/card"
import { useProfileStore } from "@/stores"
import { Button } from "../ui/button"
import {
    Dialog,
    DialogContent,
    DialogTrigger,
} from "@/components/ui/dialog"
import ExperienceForm from "./experience-form"
import { useState } from "react"
import { IconTrash } from '@tabler/icons-react';



const ExperienceProfile = () => {
    const { experiences, deleteExperienceByIndex } = useProfileStore()

    const [open, setOpen] = useState(false)

    const remoteExperience = (index: number) => {
        deleteExperienceByIndex(index)
    }

    return (
        <div className="relative ">
            <Card className="shadow-sm">
                <CardContent className="p-6">
                    <div className="flex justify-between items-center">
                        <h4 className="font-semibold text-xl line-clamp-1 break-words text-[#191919]">Experience</h4>
                        <Dialog open={open} onOpenChange={setOpen}>
                            <DialogTrigger>
                                <Button
                                    onClick={() => setOpen(true)}
                                >
                                    Add
                                </Button>
                            </DialogTrigger>
                            <DialogContent>
                                <ExperienceForm
                                    onClose={() => {
                                        setOpen(false)
                                    }}
                                />
                            </DialogContent>
                        </Dialog>


                    </div>
                    <div className="mt-3 flex flex-col gap-4">
                        {experiences.map((item, index) => (

                            <div key={`${item.company}-${item.position}`} className="w-full rounded-md bg-[#FBFAFA] flex p-4 border border-[#ecedf0] gap-4">
                                <div className="relative top-0.5 flex h-[18px] w-[18px] items-center justify-center rounded-full bg-primary">
                                    <div className="flex h-[14px] w-[14px] items-center justify-center rounded-full bg-primary text-[8px] text-white">{index + 1}</div>
                                </div>
                                <div className="flex-1 flex flex-col gap-1">
                                    <span className="text-sm font-bold">{item.position}</span>
                                    <div className="text-xs flex gap-1">
                                        <span>{item.position}</span>
                                        <span>-</span>
                                        <span>{item.company}</span>
                                    </div>
                                    <div className="text-xs flex gap-1">
                                        <span>{item.startDate}</span>
                                        <span>-</span>
                                        <span>{item.endDate}</span>
                                    </div>
                                    <span className="text-[#666666] mt-2 text-xs">
                                        {item.description}
                                    </span>
                                </div>
                                <div className="">
                                    <IconTrash
                                        size={16}
                                        className="cursor-pointer"
                                        onClick={() => remoteExperience(index)}
                                    />
                                </div>
                            </div>
                        ))}
                    </div>
                </CardContent>
            </Card>

        </div>
    )
}

export default ExperienceProfile