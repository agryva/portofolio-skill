export { default as BasicInfoEditProfile } from './basic-info.edit';
export { default as BackgroundImageEditProfile } from './background-image.edit';
export { default as AvatarImageEditProfile } from './avatar-image.edit';
export { default as ExperienceEditProfile } from './experience.edit';
