'use client'
import { Card, CardContent } from "@/components/ui/card"
import { UploadField } from "../form"
import { useProfileStore } from "@/stores"
import { useEffect, useState } from "react"

const AvatarImageEditProfile = () => {

    const { profile, setProfile } = useProfileStore()
    const [base64IMG, setBase64IMG] = useState('')

    const convertToBase64 = (selectedFile: any) => {
        const reader = new FileReader()

        reader.readAsDataURL(selectedFile)

        reader.onload = () => {
            setBase64IMG(reader.result as string)
        }
    }

    useEffect(() => {
        if (base64IMG) setProfile({ ...profile, avatarImage: base64IMG })
    }, [base64IMG])

    return (
        <Card className="shadow-sm rounded-md">
            <CardContent className="p-6">
                <h4 className="font-semibold text-xl line-clamp-1 break-words text-[#191919]">Avatar</h4>
                <hr className="mt-2" />
                <div className="mt-4">
                    <UploadField
                        fileType={['.jpeg', '.png']}
                        fileOnChange={(file) => convertToBase64(file)}
                    />
                </div>
            </CardContent>
        </Card>
    )
}

export default AvatarImageEditProfile