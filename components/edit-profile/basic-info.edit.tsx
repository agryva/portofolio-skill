'use client'
import * as yup from "yup"
import { Card, CardContent } from "@/components/ui/card"
import { FormCommon, InputForm } from "../form"
import { UseFormReturn, useForm } from "react-hook-form"
import { yupResolver } from "@hookform/resolvers/yup"
import TextAreaForm from "../form/textarea.form"
import { Button } from "../ui/button"
import { useProfileStore } from "@/stores"
import { useEffect } from "react"
import { toast } from "sonner"


const basicInfoSchema = yup.object({
    fullName: yup.string().required('Name is required'),
    title: yup.string().required('Position is required'),
    // description: yup.string().required('Description is required'),
    // about: yup.string().required('About is required'),
}).required()


const BasicInfoEditProfile = () => {

    const { profile, setProfile } = useProfileStore()

    const form = useForm<any>({
        resolver: yupResolver(basicInfoSchema),
        defaultValues: {},
    })

    const submit = async () => {
        const isValid = await form.trigger()
        if (!isValid) return
        setProfile(form.getValues())
        toast.success('Update profile successfully')
    }

    useEffect(() => {
        if (profile) {
            form.reset(profile)
        }
    }, [profile])


    return (
        <Card className="shadow-sm rounded-md">
            <CardContent className="p-6">
                <h4 className="font-semibold text-xl line-clamp-1 break-words text-[#191919]">Basic Info</h4>
                <hr className="mt-2" />
                <div className="mt-4">
                    <FormCommon
                        classNames="flex flex-col mt-4 gap-4"
                        form={form as unknown as UseFormReturn}
                        onSubmit={form.handleSubmit(submit)}
                    >
                        <div>
                            <InputForm
                                type="text"
                                name="fullName"
                                placeholder="xxxx"
                                label={'Name'}
                                className="dark:bg-[#111827]"

                            />
                        </div>
                        <div>
                            <InputForm
                                type="text"
                                name="title"
                                placeholder="xxxx"
                                label={'Position'}
                                className="dark:bg-[#111827]"
                            />
                        </div>
                        <div>
                            <TextAreaForm
                                name="description"
                                placeholder="xxxx"
                                label={'Description'}
                                className="dark:bg-[#111827]"
                            />
                        </div>
                        <div>
                            <TextAreaForm
                                name="about"
                                placeholder="xxxx"
                                label={'About'}
                                className="dark:bg-[#111827]"
                            />
                        </div>
                        <div>
                            <InputForm
                                name="websiteLink"
                                placeholder="xxxx"
                                label={'Website Link'}
                                type="link"
                                className="dark:bg-[#111827]"
                            />
                        </div>
                        <div>
                            <InputForm
                                name="portofolio"
                                placeholder="xxxx"
                                label={'Portofolio Link'}
                                type="link"
                                className="dark:bg-[#111827]"
                            />
                        </div>
                        <div>
                            <Button
                                onClick={submit}
                                type="button"
                            >
                                Save
                            </Button>
                        </div>
                    </FormCommon>
                </div>
            </CardContent>
        </Card>
    )
}

export default BasicInfoEditProfile