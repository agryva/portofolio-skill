'use client'

import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar"
import { useProfileStore } from "@/stores"
import Link from "next/link"

const HeaderProfile = () => {
    const { profile } = useProfileStore()

    return (
        <header>
            <nav className="bg-white dark:bg-gray-900 fixed w-full z-20 top-0 left-0 border-b border-gray-100">
                <div className="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4">
                    <Link
                        href="/"
                        className="flex items-center"
                    >
                        <p className="self-center text-xl font-semibold whitespace-nowrap ">Portofolio Skill </p>
                    </Link>
                    <div className="flex md:order-2">
                        <Link
                            href='/'
                            className="flex gap-2 items-center"
                        >
                            <Avatar className="w-10 h-10">
                                <AvatarImage className="object-cover" src={profile.avatarImage ?? "https://github.com/shadcn.png"} />
                                <AvatarFallback>CN</AvatarFallback>
                            </Avatar>
                            <p>{profile.fullName}</p>
                        </Link>
                    </div>

                </div>
            </nav>
        </header>

    )
}

export default HeaderProfile