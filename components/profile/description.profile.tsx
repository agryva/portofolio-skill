'use client'

import {
    Card,
    CardContent,
} from "@/components/ui/card"
import { useProfileStore } from "@/stores"


const DescriptionProfile = () => {
    const { profile } = useProfileStore()
    return (
        <div className="relative ">
            <Card className="shadow-sm">
                <CardContent className="p-6">
                    <h4 className="font-semibold text-xl line-clamp-1 break-words text-[#191919]">About</h4>
                    <p className="mt-3 text-sm">
                        {profile?.about || '-'}
                    </p>
                </CardContent>
            </Card>

        </div>
    )
}

export default DescriptionProfile