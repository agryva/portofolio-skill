'use client'

import {
    Card,
    CardContent,
} from "@/components/ui/card"
import { Badge } from "@/components/ui/badge"



const SkillsProfile = () => {
    return (
        <div className="relative ">
            <Card className="shadow-sm">
                <CardContent className="p-6">
                    <h4 className="font-semibold text-xl line-clamp-1 break-words text-[#191919]">Skills</h4>
                    <p className="mt-3 text-sm flex flex-wrap gap-2">
                        <Badge variant="outline">Frontend</Badge>
                        <Badge variant="outline">Backend</Badge>
                        <Badge variant="outline">Software Engineer</Badge>
                        <Badge variant="outline">Kotlin</Badge>
                        <Badge variant="outline">React.js</Badge>
                        <Badge variant="outline">Next js</Badge>
                        <Badge variant="outline">Node js</Badge>
                        <Badge variant="outline">Laravel</Badge>
                        <Badge variant="outline">Vue.js</Badge>
                    </p>
                </CardContent>
            </Card>

        </div>
    )
}

export default SkillsProfile