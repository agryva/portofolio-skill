'use client'

import {
    Card,
    CardContent,
} from "@/components/ui/card"
import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar"
import Image from "next/image"
import { Button, buttonVariants } from "../ui/button"
import { IconEditCircle } from '@tabler/icons-react';
import { cn } from "@/lib/utils";
import { useProfileStore } from "@/stores"
import Link from "next/link"


const HeroProfile = ({ isPreview }: { isPreview?: boolean }) => {
    const { profile } = useProfileStore()

    console.log('asdasda', (profile.websiteLink !== '') ? '' : 'hidden')

    return (
        <div className="relative ">
            <Card className="shadow-sm">
                <CardContent className="p-0">
                    <div className="w-full h-[190px] relative">
                        <Image
                            src={profile?.backgroundImage ?? 'https://img.cakeresume.com/cdn-cgi/image/width=1920,quality=75,format=auto/https://next-assets.cakeresume.com/_next/static/media/cover-profile-default.edf18f92.png'}
                            alt="banner"
                            fill
                            className="object-cover rounded-tl-lg rounded-tr-lg"
                        />

                        {!isPreview && (

                            <div className="absolute top-0 lef-0 right-0 p-4 bg-transparent">
                                <Link
                                    className={buttonVariants()}
                                    href='/edit'
                                >
                                    <IconEditCircle />
                                </Link>
                            </div>
                        )}
                    </div>
                    <div className="p-6">
                        <div className={cn("flex md:flex-row flex-col gap-4", isPreview && 'md:flex-col')}>
                            <Avatar className="w-40 h-40">
                                <AvatarImage className="object-cover" src={profile?.avatarImage ?? "https://github.com/shadcn.png"} />
                                <AvatarFallback>CN</AvatarFallback>
                            </Avatar>
                            <div className="flex-1 flex-col flex justify-center">
                                <h4 className="font-semibold text-2xl line-clamp-1 break-words text-[#191919]">{profile?.fullName ?? '-'}</h4>
                                <p className="text-md font-normal">{profile?.title ?? '-'}</p>
                                <span className="text-[#666666] font-light text-sm">{profile?.description ?? '-'}</span>
                            </div>
                            <div className="flex gap-4">
                                <Link
                                    className={cn(buttonVariants(), profile.portofolio !== '' ? '' : 'hidden',)}
                                    href={profile.portofolio ?? '#'}
                                >
                                    Portofolio
                                </Link>
                                <Link
                                    className={cn(buttonVariants({ variant: 'outline' }), (profile.websiteLink !== '') ? '' : 'hidden')}
                                    href={profile.websiteLink ?? '#'}
                                >
                                    Website
                                </Link>
                            </div>
                        </div>
                    </div>
                </CardContent>
            </Card>

        </div>
    )
}

export default HeroProfile