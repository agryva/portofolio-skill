'use client'

import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar"
import { Card, CardContent } from "@/components/ui/card"
import { useProfileStore } from "@/stores"

const ExperienceProfile = () => {
    const { experiences } = useProfileStore()
    return (
        <div className="relative ">
            <Card className="shadow-sm">
                <CardContent className="p-6">
                    <h4 className="font-semibold text-xl line-clamp-1 break-words text-[#191919]">Experience</h4>
                    <div className="flex flex-col mt-3 gap-4">

                        {experiences.map((item, index) => (
                            <div key={`${item.company}-${item.position}`} className="flex flex-col ">
                                <div className="flex gap-4">
                                    <div className="relative top-0.5 flex h-[18px] w-[18px] items-center justify-center rounded-full bg-primary">
                                        <div className="flex h-[14px] w-[14px] items-center justify-center rounded-full bg-primary text-[8px] text-white">{index + 1}</div>
                                    </div>
                                    <div className="flex-1 flex-col flex justify-center">
                                        <h4 className="font-semibold text-base line-clamp-1 break-words text-[#191919]">{item.position}</h4>
                                        <p className="text-sm font-normal">{item.company}</p>
                                        <div className="text-xs flex gap-1 mt-1">
                                            <span>{item.startDate}</span>
                                            <span>-</span>
                                            <span>{item.endDate}</span>
                                        </div>
                                        <span className="text-[#666666] mt-2 text-xs">
                                            {item.description}
                                        </span>
                                    </div>
                                </div>
                                <hr className="mt-4" />
                            </div>
                        ))}

                    </div>
                </CardContent>
            </Card>

        </div>
    )
}

export default ExperienceProfile