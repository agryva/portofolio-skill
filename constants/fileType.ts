export const FileTypeCustom: { [key: string]: any } = {
  IMAGES: {
    'image/*': ['.jpeg', '.jpg', '.png', '.gif', '.svg'],
  },
  PDF: {
    'application/pdf': ['.pdf'],
  },
  EXCEL: {
    'application/vnd.ms-excel': ['.xls', '.xlsx'],
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': [
      '.xls',
      '.xlsx',
    ],
  },
  DOCX: {
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document': [
      '.doc',
      '.docx',
    ],
    'application/vnd.openxmlformats-officedocument.wordprocessingml.template': [
      '.doc',
      '.docx',
    ],
  },
  PPTX: {
    'application/vnd.ms-powerpoint': ['.ppt', '.pptx'],
    'application/vnd.ms-powerpoint.presentation.macroEnabled.12': [
      '.ppt',
      '.pptx',
    ],
    'application/vnd.openxmlformats-officedocument.presentationml.presentation':
      ['.ppt', '.pptx'],
  },
  CSV: {
    'text/csv': ['.csv'],
  },
  ZIP: {
    'application/zip': ['.zip'],
  },
  RAR: {
    'application/rar': ['.rar'],
  },
  AUDIO: {
    'audio/*': ['.mp3'],
  },
  VIDEO: {
    'video/*': ['.mp4', '.mov'],
  },
}
