import { HeaderProfile } from "@/components/profile"
import { ReactNode } from "react"



const LayoutProfile = ({ children }: { children: ReactNode }) => {
    return (
        <main>
            <HeaderProfile />
            {children}
        </main>
    )
}

export default LayoutProfile