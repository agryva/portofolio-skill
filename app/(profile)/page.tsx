import { DescriptionProfile, ExperienceProfile, HeroProfile, SkillsProfile } from "@/components/profile"

const ProfilePage = () => {
    return (
        <div className="max-w-7xl mx-auto p-4 flex flex-col gap-4 mt-[80px]">
            <HeroProfile />
            <div className="flex flex-col md:grid grid-cols-12 gap-4">
                <div className="col-span-8 flex flex-col gap-4">
                    <DescriptionProfile />
                    <ExperienceProfile />
                </div>
                <div className="col-span-4">
                    <SkillsProfile />
                </div>
            </div>

        </div>
    )
}

export default ProfilePage