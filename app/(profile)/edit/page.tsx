import { AvatarImageEditProfile, BackgroundImageEditProfile, BasicInfoEditProfile, ExperienceEditProfile } from "@/components/edit-profile"
import { DescriptionProfile, ExperienceProfile, HeroProfile } from "@/components/profile"

const ProfileEditPage = () => {
    return (
        <div className="max-w-7xl mx-auto p-4 flex flex-col gap-4 mt-[80px]">
            <div className="flex flex-col md:grid grid-cols-12 gap-4">
                <div className="col-span-7 flex flex-col gap-4">
                    <BackgroundImageEditProfile />
                    <AvatarImageEditProfile />
                    <BasicInfoEditProfile />
                    <ExperienceEditProfile />
                </div>
                <div className="col-span-5 flex flex-col gap-4">
                    <HeroProfile isPreview />
                    <DescriptionProfile />
                    <ExperienceProfile />
                </div>
            </div>
        </div>
    )
}

export default ProfileEditPage