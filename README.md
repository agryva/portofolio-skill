# Portofolio Skills

This is a Next.js project showcasing various skills for a portfolio.

### Prerequisites

Make sure you have [Node.js](https://nodejs.org/) and [PNPM](https://pnpm.io/) installed on your machine.

## Getting Started

Make sure you have [Node.js](https://nodejs.org/) and [PNPM](https://pnpm.io/) installed on your machine.

- To install PNPM, you can use npm (Node.js package manager):

  ```sh
  npm install -g pnpm
  ```

- Running in your local

  ```bash
  pnpm dev
  ```

Open [http://localhost:3411](http://localhost:3411) with your browser to see the result.

## Data Schema

![schema](/screenshot//schema.jpg 'Schema')
