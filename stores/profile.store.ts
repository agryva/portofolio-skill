import { IExperience, IProfile } from '@/interfaces/profile.interface';
import { create } from 'zustand';
import { persist } from 'zustand/middleware';

interface ProfileStoreInterface {
  profile: IProfile;
  experiences: IExperience[];
  setProfile: (item: IProfile | undefined) => void;
  addExperience: (item: IExperience | undefined) => void;
  deleteExperienceByIndex: (index: number) => void
}

const useProfileStore = create(
  persist<ProfileStoreInterface>(
    (set, get) => ({
      profile: {
        fullName: 'Example',
        title: 'Software Engineer',
      },
      experiences: [],
      setProfile: (item) => {
        if (item) {
          set({ profile: item });
        }
      },
      addExperience: (item) => {
        const experience = get().experiences;
        if (item) {
          experience.push(item);
          set({ experiences: experience });
        }
      },
      deleteExperienceByIndex: (index) => {
        const experience = get().experiences;
        if (index >= 0 && index < experience.length) {
          experience.splice(index, 1);
          set({ experiences: experience });
        }
      },
    }),
    {
      name: `profile`,
    }
  )
);

export default useProfileStore;
