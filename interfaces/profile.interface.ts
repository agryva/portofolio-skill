export interface IProfile {
  backgroundImage?: string;
  avatarImage?: string;
  fullName: string;
  title: string;
  description?: string;
  about?: string;
  websiteLink?: string;
  portofolio?: string;
}

export interface IExperience {
  position: string;
  company: string;
  startDate: string;
  endDate: string;
  description: string;
}
