import { ReactNode } from "react";

export interface FormGeneralProps {
  name: string;
  label?: string | ReactNode;
  labelClassName?: string;
  placeholder?: string;
  className?: string;
  description?: string;
  type?: string;
}
