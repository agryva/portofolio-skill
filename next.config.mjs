/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        remotePatterns: [
            {
                protocol: 'https',
                hostname: 'img.cakeresume.com',
                port: '',
            },
        ],
    },
};

export default nextConfig;
